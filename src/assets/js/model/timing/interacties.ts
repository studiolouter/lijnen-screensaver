import { AnimationSettings } from './timings'

interface InteractiesTiming {
  lijnen: AnimationSettings,
  // circles: AnimationSettings,
  // other: AnimationSettings,
  lijnen_side: AnimationSettings
}

export const interactiesTiming: InteractiesTiming = {
  lijnen: {
    delay: {
      stagger: 10,
      before: 1000,
      firstTime: 1000
    },
    break: 1000,
    duration: 1000,
    circleBackAnimation: {
      stagger: 10,
      duration: 1000
    },
    hasCircles: true
  },
  // circles: {
  //   delay: {
  //     stagger: 10,
  //     before: 1000,
  //     firstTime: 1000
  //   },
  //   break: 1000,
  //   duration: 1000,
  //   type: 'opacity'
  // },
  // other: {
  //   delay: {
  //     stagger: 10,
  //     before: 1000,
  //     firstTime: 1000
  //   },
  //   break: 1000,
  //   duration: 1000
  // },
  lijnen_side: {
    delay: {
      stagger: 10,
      before: 1000,
      firstTime: 3000
    },
    break: 1000,
    duration: 1000
  }
}
