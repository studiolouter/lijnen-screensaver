class Themas {
  available: Array<string>
  current: string

  constructor () {
    this.available = ['overvloed', 'verwantschappen', 'verstoringen', 'habitats', 'interacties' ]
  }

  setCurrent (zoneToSet: string) {
    this.current = zoneToSet
  }
}

export const thema = new Themas()
