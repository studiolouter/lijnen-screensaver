import anime from 'animejs'

class ScreensaverController {
  container: HTMLElement
  defaultFadeDuration: number

  constructor () {
    this.container = null
    this.defaultFadeDuration = 300
  }

  setup (container: HTMLElement) {
    this.container = container
  }

  setDefaultFadeDuration (duration: number) {
    this.defaultFadeDuration = duration
  }

  show (duration: number = this.defaultFadeDuration) {
    return new Promise((resolve, reject) => {
      anime({
        targets: this.container,
        opacity: 1,
        duration: duration,
        easing: 'easeInOutSine',
        complete: () => {
          resolve(true)
        }
      })
    })
  }

  hide (duration: number = this.defaultFadeDuration) {
    // console.log(duration)
    return new Promise((resolve, reject) => {
      anime({
        targets: this.container,
        opacity: 0,
        duration: duration,
        easing: 'easeInOutSine',
        complete: () => {
          resolve(true)
        }
      })
    })
  }
}

export const screensaverController = new ScreensaverController()
