import { lijnenView } from '../views/Lijnen.view'
import { timings } from '../model/timing/timings'
import anime from 'animejs'

class LijnenController {
  setupCompleted: boolean
  animationReferences: Array<{ pause: Function, play: Function }>
  shouldPausePlayback: boolean
  fadeDuration: number
  zone: string
  constructor () {
    this.setupCompleted = false
    this.animationReferences = []
    this.shouldPausePlayback = true
    this.fadeDuration = 300
  }

  setShouldPausePlayback (state: boolean) {
    this.shouldPausePlayback = state
  }

  setDefaultFadeDuration (duration: number) {
    this.fadeDuration = duration
  }

  setup (zone: string) {
    this.zone = zone
    Object.keys(lijnenView.elements).forEach(key => {
      lijnenView.makeActive(key)
      this.animate(key, true)
    })
    // console.log(lijnenView.elements)
    this.setupCompleted = true
  }

  animate (target: string, firstTime: boolean = false) {
    // if (target !== 'habitats') firstTime = true
    if (timings[target].type === 'opacity') {
      this.animations().opacity(target, firstTime)
    } else {
      this.animations().stroke(target, firstTime)
    }
  }

  animations () {
    return {
      stroke: (target, firstTime: boolean = false) => {
        let afterTime = timings[target].delay.before
        if (firstTime) afterTime = timings[target].delay.firstTime
        const waitTime = timings[target].wait ? timings[target].wait : 0
        this.animationReferences.push(anime({
          targets: lijnenView.elements[target],
          strokeDashoffset: (el: HTMLElement) => { if (el.tagName !== 'circle') { return [anime.setDashoffset, 0] } },
          opacity: (e: HTMLElement) => { if (e.tagName === 'circle' || e.tagName === 'path') { return 1 } },
          easing: 'easeInOutSine',
          duration: timings[target].duration,
          delay: (el: HTMLElement, i: number) => { return (i * timings[target].delay.stagger) + afterTime },
          complete: (anim) => {
            this.removeFromAnimations(anim)
            let targets = null
            if (target === 'habitats' || this.zone === 'verwantschappen') {
              targets = [].slice.call(lijnenView.elements[target]).reverse()
            } else {
              targets = lijnenView.elements[target]
            }
            this.animationReferences.push(anime({
              targets: targets,
              strokeDashoffset: (el: HTMLElement) => { if (el.tagName !== 'circle') { return [0, anime.setDashoffset] } },
              opacity: (e: HTMLElement) => { if (e.tagName === 'circle') { return 0 } },
              easing: 'easeInOutSine',
              duration: (el: HTMLElement) => {
                if ((el.tagName === 'circle' || el.tagName === 'path' || el.classList.contains('st4') || el.classList.contains('st5') || el.classList.contains('st6') || el.classList.contains('st7') || el.classList.contains('st8') || el.classList.contains('st9')) && timings[target].hasCircles === true) {
                  return timings[target].circleBackAnimation.duration
                } else {
                  return timings[target].duration
                }
              },
              delay: (el: HTMLElement, i: number) => {
                if ((el.tagName === 'circle' || el.tagName === 'path' || el.classList.contains('st4') || el.classList.contains('st5') || el.classList.contains('st6') || el.classList.contains('st7') || el.classList.contains('st8') || el.classList.contains('st9')) && timings[target].hasCircles === true) {
                  return (i * timings[target].delay.stagger + timings[target].break)
                } else {
                  return (i * timings[target].delay.stagger + timings[target].break)
                }
              },
              complete: (anim) => {
                if (target === 'habitats') {
                  setTimeout(() => {
                    this.removeFromAnimations(anim)
                    this.animate(target)
                  }, waitTime)
                } else {
                  this.removeFromAnimations(anim)
                  this.animate(target)
                }
              }
            }))
          }
        }))
      },
      opacity: (target, firstTime: boolean = false) => {
        let afterTime = timings[target].delay.before
        if (firstTime) afterTime = timings[target].delay.firstTime
        this.animationReferences.push(anime({
          targets: lijnenView.elements[target],
          opacity: 1,
          easing: 'easeInOutSine',
          duration: timings[target].duration,
          delay: (el: HTMLElement, i: number) => { return (i * timings[target].delay.stagger) + afterTime },
          complete: (anim) => {
            this.removeFromAnimations(anim)
            this.animationReferences.push(anime({
              targets: lijnenView.elements[target],
              opacity: 0,
              easing: 'easeInOutSine',
              duration: timings[target].duration,
              delay: (el: HTMLElement, i: number) => { return (i * timings[target].delay.stagger + timings[target].break) },
              complete: (anim) => {
                this.removeFromAnimations(anim)
                this.animate(target, false)
              }
            }))
          }
        }))
      }
    }
  }

  removeFromAnimations (animation) {
    this.animationReferences.forEach((a, i) => {
      if (a === animation) this.animationReferences.splice(i, 1)
    })
  }

  hide (duration: number = this.fadeDuration) {
    for (let index in lijnenView.elements) {
      const element = lijnenView.elements[index]
      anime({
        targets: element,
        easing: 'easeInOutSine',
        duration: duration,
        opacity: 0,
        complete: () => {
          this.pause()
        }
      })
    }
  }

  show (duration: number = this.fadeDuration) {
    this.play()
    for (let index in lijnenView.elements) {
      const element = lijnenView.elements[index]
      anime({
        targets: element,
        easing: 'easeInOutSine',
        duration: duration,
        opacity: 1,
        complete: () => {
          return true
        }
      })
    }
  }

  pause () {
    this.animationReferences.forEach(animation => {
      animation.pause()
    })
  }

  play () {
    this.animationReferences.forEach(animation => {
      animation.play()
    })
  }
}

export const lijnenController = new LijnenController()
