// import { titles } from '../model/titles'
class TitlesView {
  container: HTMLElement
  titleDiv: HTMLElement

  constructor () {
    this.container = null
    this.titleDiv = null
  }

  setup (wrapper: HTMLElement) {
    const container = document.createElement('div')
    container.classList.add('screensaver-title-container')
    this.titleDiv = document.createElement('span')
    this.titleDiv.classList.add('title-div')
    container.appendChild(this.titleDiv)
    wrapper.appendChild(container)
  }
}

export const titlesView = new TitlesView()
