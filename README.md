# Lijnen Screensaver

### Deze package kan gebruikt worden om in de Brussel Levende Plaaneet projecten een lijnen screensaver toe te voegen


## Installeren
1. Voeg deze package als dependency toe door `yarn add lijnenscreensaver` of `npm i lijnenscreensaver` uit te voeren in de folder van je huidige project

## Updaten
Deze package is nog in ontwikkeling, twee zone's zijn gebruiksklaar (overvloed & verstoringen), de resterende animaties volgen nog. Als er een nieuwe versie klaar staat voer dan het volgende commando uit om de laatste versie te downloaden 

```shell
npm i lijnenscreensaver@latest
```

## Importeren
### 1.
`import` of `require` vervolgens de gedownloade package in je project.

```javascript
import { lijnenScreensaver } from 'lijnenscreensaver'
```

of

```javascript
const { lijnenScreensaver } = require('lijnenscreensaver')
```

### 2.
Vervolgens moet de screensaver worden geinitialiseerd door een HTMLElement te voorzien aan de setup van de screensaver. Doe dit in de setup van je eigen project, voordat er andere animaties etc. starten.
```javascript
const wrapper = document.getElementById('wrapper')
lijnenScreensaver.setup(wrapper)
```

### 3.
Vervolgens heeft de screensaver een zone nodig zodat de goede animatie opgehaald kan worden, dit doe je door de `setZone(string)` functie uit te voeren op de lijnenScreensaver. 
```javascript
lijnenScreensaver.setZone('overvloed')
```

### 4.
Vervolgens kun je de titels toevoegen die worden getoond in de screensaver, dat doe je door de functie `setTitles(titles)` uit te voeren. Deze functie heeft een object nodig die als volgt is gestructureerd:
```json
{
  nl: "string",
  en: "string",
  de: "string",
  fr: "string"
}
```

De voorgaande functies kan ook meteen worden uitgevoerd in dezelfde regel als je de setup initieert. Dan ziet de totale code er als volgt uit
```javascript
const wrapper = document.getElementById('wrapper')
lijnenScreensaver
  .setup(wrapper)
  .setZone('overvloed')
  .setTitles({ nl: "string", en: "string", de: "string", fr: "string"})
```

## Gebruik

De label animatie heeft een bepaalde timing, deze timing kan worden bekeken, maar kan ook op worden ingehaakt via callbacks.

### `getAnimationDetails().timing()`

Door `getAnimationDetails().timing()` uit te voeren krijg je een object terug waarin de aniamtie timing wordt aangegeven.

### `getAnimationDetails().sequence()`

door `getAnimationDetails().sequence()` uit te voeren krijg je een array terug met daarin de volgorde waarin de animatie sequentie wordt uitgevoerd.

---

Er zijn verschillende callbacks ingebouwd waardoor je de status van de screensaver kan uitlezen. Via de `.on(id, () => {})` functie kan geluisterd worden naar verschillende evenementen.

### `on('titleChange', languageCode => {})`
Door de `on()` functie uit te voeren met en `id` van `titleChange` zal er een callback worden uitgevoerd die een string returned met de taalcode. Deze callback word uitgevoerd wanneer de taal van de screensaver albels veranderd. Deze switch gebeurd na het uitfaden van een eventueel vorige label, en voordat deze in fade. Een voorbeeld van de code

```javascript
const wrapper = document.getElementById('wrapper')
lijnenScreensaver
  .setup(wrapper)
  .setZone('overvloed')
  .setTitles({ nl: "string", en: "string", de: "string", fr: "string"})
  .on('titleChange', languageCode => {
    console.log(languageCode)
  })
```

Bonus: je kan ook de vlag emoji opvragen door een extra `flags` parameter toe te voegen. Ex:
```javascript
.on('titleChange', (languageCode, flag) => {
  console.log(flag, languageCode)
})
```

### `.on('labelStateChange', (state, animation, duration) => { }`
De state veranderingen van de animatie kunnen worden bijgehouden door te luisteren naar events met id `labelStateChange`. De callback bestaat uit 2(+1 optioneel) parameters: status van de animatie(zijnde `start` of `completed`) & de naam van de animatie (zijnde `wait`, `fadeIn`, `fadeOut` of `setPosition`). De derde parameter wordt alleen toegevoegd als het de start van een animatie betreft, dan zal ook de duur van de animatie worden meegestuurd. Een voorbeeld.
```javascript
const wrapper = document.getElementById('testInclude')
  lijnenScreensaver
  .setup(wrapper)
  .setZone('overvloed')
  .setTitles({ nl: 'string', en: 'string', de: 'string', fr: 'string' })
  .on('labelStateChange', (state, animation, duration) => {
    const conditionalDurationString = duration ? 'which will take ' + duration + ' miliseconds' : ''
    console.log(
      animation,
      "changed it's state to:",
      state,
      conditionalDurationString
    )
  })
```


## Configuren

De screensaver heeft een bepaalde configuratie die overschreven kan worden met eigen variabellen, mocht dat gewenst zijn.

### `setShouldPausePlayback(true|false)`
Wanneer je de animatie pauzeert (zie [pause()](#pause)) wordt na de fade de animatie gepauzeert. Wil je dat de animatie door blijft spelen wanneer je de screensaver [hide](#hide) voer dan de functie `setShouldPausePlayback(true|false)` uit met een `false` waarde.

### `setDefaultFadeDuration(duration)`
De [hide](#hide) animatie is standaard ingesteld op een animatie duur van 300 miliseconden. Deze default tijd kan worden ingesteld door de functie `setDefaultFadeDuration(duration)` uit te voeren. De `duration` moet in miliseconden worden doorgegeven.

# For development
Als je de gelukkige bent een animatie toe te voegen aan de collectie dan kun je dat doen door de volgende bestanden aan te passen

### src/assets/js/model/thema.ts
Check of het thema waarmee je aan de slag gaat in de lijst staat van available, zet het er anders bij.

### src/assets/svg
Maak een SVG, en voeg die toe in deze map

### src/assets/js/model/svg.ts
1. Importeer de zojuist toegevoegde SVG, voeg die vervolgens toe in de `switch`.
2. Voeg aan de `returnSvg(zone)` de geimporteerde SVG toe met dezlfde *key* die je hebt gebruikt bij de `available` variabele in *thema.ts*
3. Voeg de verschillende HTMLElementen uit de SVG toe die je wilt animeren.

### src/assets/js/model/timing/*
Voeg toe, of pas aan, de timing file om verschillende variabelen mee te geven aan de animatie.

Gebruik dezelfde keys als je in *svg.ts* hebt gebruikt.  

De timing variabelen bestaan uit:

`delay.stagger`: De tijd tussen elk element van een verschillende laag in miliseconden

`delay.before`: 1000: De tijd voordat de animatie begint in miliseconden

`delay.firstTime`: De tijd die alleen de eerste keer dat de aniamtie begint voorkomt in miliseconde.

`after`: De tijd hoe lang de animatie wacht voordat deze opnieuw begint in miliseconde.

`duration`: De tijd die de animatie (per element) duurt in miliseconde.
