import './thema'
import svgOvervloed from '../../svg/overvloed-07.svg'
import svgVerstoringen from '../../svg/verstoringen01_1-01.svg'
import svgInteracties from '../../svg/interacties-01_01.svg'
import svgHabitats from '../../svg/habitat01_01.svg'
import svgVerwantschappen from '../../svg/verwantschappen01_01.svg'

class Svg {
  returnSvg = (zone: string) => {
    switch (zone) {
      case 'overvloed':
        return svgOvervloed
      case 'verstoringen':
        return svgVerstoringen
      case 'interacties':
        return svgInteracties
      case 'habitats':
        return svgHabitats
      case 'verwantschappen':
        return svgVerwantschappen
      default:
        break
    }
  }

  getElements (zone: string) {
    switch (zone) {
      case 'overvloed':
        return {
          lines1: document.querySelectorAll('#Laag_1 .cls-1'),
          lines1_c: document.querySelectorAll('#Laag_1_kopie .cls-1'),
          circles: document.querySelectorAll('#Laag_3_kopie .cls-1'),
          circles_c: document.querySelectorAll('#Laag_3 .cls-1')
        }
      case 'verwantschappen':
        return {
          lijnen0: document.querySelectorAll('.lijnenscreensaver-svg > #LIJNEN > *'),
          lijnen1: document.querySelectorAll('.lijnenscreensaver-svg > #LIJNEN2 > *'),
          lijnen2: document.querySelectorAll('.lijnenscreensaver-svg > #LIJNEN3 > *'),
          lijnen3: document.querySelectorAll('.lijnenscreensaver-svg > #LIJNEN4 > *')
          // lijnen1: document.querySelectorAll('.lijnenscreensaver-svg > #Laag_2 > .LIJNEN > *')
        }
      case 'verstoringen':
        return {
          lijnen_links: document.querySelectorAll('#Lijnen01 path, #Lijnen01 circle'),
          lijnen_rechts: document.querySelectorAll('#Lijnen02 path, #Lijnen02 circle'),
          circles01: document.querySelectorAll('#Circles_1_ #c01 circle, #Circles_1_ #c01 path'),
          circles02: document.querySelectorAll('#Circles_1_ #c02 circle, #Circles_1_ #c02 path'),
          circles03: document.querySelectorAll('#Circles_1_ #c03 circle, #Circles_1_ #c03 path'),
          circles04: document.querySelectorAll('#Circles_1_ #c04 circle, #Circles_1_ #c04 path'),
          circles05: document.querySelectorAll('#Circles_1_ #c05 circle, #Circles_1_ #c05 path'),
          circles06: document.querySelectorAll('#Circles_1_ #c06 circle, #Circles_1_ #c06 path'),
          circles07: document.querySelectorAll('#Circles_1_ #c07 circle, #Circles_1_ #c07 path')
        }
      case 'habitats':
        return {
          lijnen0: document.querySelectorAll('.lijnenscreensaver-svg > #Laag_1 > .LIJNEN > *'),
          lijnen1: document.querySelectorAll('.lijnenscreensaver-svg > #Laag_2 > .LIJNEN > *')
          // lijnen2: document.querySelectorAll('.lijnenscreensaver-svg > #Laag_3 > .LIJNEN > *'),
          // circles: document.querySelectorAll('.lijnenscreensaver-svg > #Laag_1 > #CIRCLES > *'),
          // circles: document.querySelectorAll('.lijnenscreensaver-svg > #Laag_1 > #CIRCLES > path')
        }
      case 'interacties':
        return {
          lijnen: document.querySelectorAll('#Layer_1 > *'),
          // circles: document.querySelectorAll('#Layer_11 .st1'),
          // other: document.querySelectorAll('#Layer_11 .st2'),
          lijnen_side: document.querySelectorAll('#Layer_2 > *')
        }
      default:
        break
    }
  }
}

export const svg: Svg = new Svg()
