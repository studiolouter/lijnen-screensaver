import './assets/sass/main.scss'

import { screensaverController } from './assets/js/controllers/screensaverController'

import { lijnenController } from './assets/js/controllers/Lijnen.controller'
import { lijnenView } from './assets/js/views/Lijnen.view'
import { thema } from './assets/js/model/thema'
import { svg } from './assets/js/model/svg'

import { titles, ITitles } from './assets/js/model/titles'
import { titlesController } from './assets/js/controllers/Titles.controller'
import { titlesView } from './assets/js/views/Titles.view'

import { timings } from './assets/js/model/timing/timings'

class LijnenScreensaver {
  container: HTMLElement
  play: Function
  pause: Function
  cb: Function

  constructor () {
    this.container = null
    this.play = function () { lijnenController.play() }
    this.pause = function () { lijnenController.pause() }
    this.cb = null
  }

  setup (wrapper: HTMLElement): LijnenScreensaver {
    if (wrapper instanceof HTMLElement === false) throw new Error('Moet een HTMLElement krijgen om de view aan toe te voegen')
    this.container = document.createElement('lijnen-screensaver')
    this.container.classList.add('lijnen-screensaver-container')
    wrapper.appendChild(this.container)
    screensaverController.setup(this.container)
    return this
  }

  setZone (zone: string): LijnenScreensaver {
    if (!validate.knownThema(zone)) throw new Error('Thema wordt niet herkend')
    thema.setCurrent(zone)
    timings.setTimingOfZone(zone)
    const svgToInclude = svg.returnSvg(zone)
    lijnenView.setup(this.container, svgToInclude)
    validate.checkTimingAndElementKeys()
    lijnenController.setup(zone)
    return this
  }

  setTitles (data: ITitles) {
    if (!validate.allLanguagesPresent(data)) throw new Error('De titles die zijn doorgegeven zijn niet goed geformateerd')
    titles.setup(data)
    titlesView.setup(this.container)
    titlesController.setup()
    titlesController.startMoveAndFadeAnimation().then().catch()
    return this
  }

  async show (duration: number) {
    if (lijnenController.shouldPausePlayback) lijnenController.play()
    if (titlesController.shouldPausePlayback) titlesController.play()
    return screensaverController.show()
  }

  async hide (duration: number) {
    await screensaverController.hide()
    if (lijnenController.shouldPausePlayback) lijnenController.pause()
    if (titlesController.shouldPausePlayback) titlesController.pause()
  }

  setDefaultFadeDuration (duration: number) {
    screensaverController.setDefaultFadeDuration(duration)
  }

  setShouldPausePlayback (state: boolean) {
    lijnenController.setShouldPausePlayback(state)
    titlesController.setShouldPausePlayback(state)
  }

  on (id: string, cb: Function) {
    if (id === 'titleChange') {
      titlesController.titleHasChanged = cb
    } else if (id === 'labelStateChange') {
      titlesController.stateChange = cb
    } else {
      throw new Error('De id van de on event listener bestaat niet')
    }
    return this
  }

  getAnimationDetails () {
    return {
      timing: () => { return titles.timing },
      sequence: () => { return titles.animationSequence }
    }
  }
}

class Validate {
  knownThema (themaRequested: string) {
    let bToReturn = false
    thema.available.forEach(t => {
      if (t === themaRequested) bToReturn = true
    })
    return bToReturn
  }

  allLanguagesPresent (titles: ITitles) {
    if (titles.nl == null || titles.en == null || titles.fr == null || titles.de == null) {
      return false
    } else return true
  }

  checkTimingAndElementKeys () {
    const arr1 = Object.keys(lijnenView.elements).concat().sort()
    const arr2 = Object.keys(timings).concat().sort()

    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) throw new Error('De keys die zijn gebruikt in timings.ts en svgElement.ts zijn niet hetzelfde, dat moet wel')
    }
    return true
  }
}

const validate = new Validate()
export const lijnenScreensaver = new LijnenScreensaver()
