import { svg } from '../model/svg'
import { thema } from '../model/thema'

class LijnenView {
  setupCompleted: boolean
  container: HTMLElement
  elements: object
  constructor () {
    this.setupCompleted = false
    this.container = null
    this.elements = null
  }

  setup (wrapper: HTMLElement, svgToInclude: string) {
    wrapper.innerHTML = svgToInclude
    this.elements = svg.getElements(thema.current)
    for (let element in this.elements) {
      this.elements[element].forEach(el => {
        el.classList.add('inactive')
      })
    }
  }

  makeActive (element: string) {
    this.elements[element].forEach(el => {
      el.classList.remove('inactive')
    })
  }

  async hide () {
    for (let index in this.elements) {
      const elements: NodeList = this.elements[index]
      elements[0].parentElement.classList.add('hidden')
    }
  }

  show () {
    for (let index in this.elements) {
      const elements: NodeList = this.elements[index]
      elements[0].parentElement.classList.remove('hidden')
    }
  }
}

export const lijnenView = new LijnenView()
