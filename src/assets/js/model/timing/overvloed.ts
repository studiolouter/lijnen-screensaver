import { AnimationSettings } from './timings'

interface OvervloedTiming {
  lines1: AnimationSettings,
  lines1_c: AnimationSettings,
  circles: AnimationSettings,
  circles_c: AnimationSettings
}

export const overvloedTiming: OvervloedTiming = {
  lines1: {
    delay: {
      stagger: 5,
      before: 3000,
      firstTime: 0
    },
    break: 0,
    duration: 2600
  },
  lines1_c: {
    delay: {
      stagger: 5,
      before: 700,
      firstTime: 0
    },
    break: 0,
    duration: 2300
  },
  circles: {
    delay: {
      stagger: 5,
      before: 2000,
      firstTime: 0
    },
    break: 0,
    duration: 2000
  },
  circles_c: {
    delay: {
      stagger: 250,
      before: 2000,
      firstTime: 0
    },
    break: 0,
    duration: 2000
  }
}
