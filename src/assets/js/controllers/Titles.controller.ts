import { titles } from '../model/titles'
import { titlesView } from '../views/Titles.view'
import anime from 'animejs'

class TitlesController {
  animation: { pause: Function, play: Function }
  container: HTMLElement
  languageIndex: number
  lastPosition: {x: number, y: number}
  shouldPausePlayback: boolean
  titleHasChanged: Function
  stateChange: Function

  constructor () {
    this.lastPosition = { x: 0, y: 0 }
    this.shouldPausePlayback = true
    this.animation = null
    this.titleHasChanged = null
    this.stateChange = null
  }

  setup () {
    this.languageIndex = 0
    const data = titles.data
  }

  setShouldPausePlayback (status: boolean) {
    this.shouldPausePlayback = status
  }

  increaseLanguageIndex () {
    this.languageIndex++
    if (this.languageIndex > Object.keys(titles.data).length - 1) this.languageIndex = 0
    this.changeText()
  }

  changeText () {
    titlesView.titleDiv.innerHTML = titles.data[titles.languages[this.languageIndex]]
  }

  play () {
    if (this.animation) this.animation.play()
  }

  pause () {
    if (this.animation) this.animation.pause()
  }

  async startMoveAndFadeAnimation () {
    this.moveAndFadeAnimation().then(() => {
      this.startMoveAndFadeAnimation().catch(e => console.error(e))
    }).catch(e => console.error(e))
  }

  async moveAndFadeAnimation () {
    this.increaseLanguageIndex()

    const startPosition = this.getRandomPosition()
    const targetPosition = this.getRandomPosition(true)

    await this.setPosition(startPosition, 0)
    await this.fadeIn(titles.timing.fadeIn)
    await this.milis(titles.timing.wait.start)
    await this.setPosition(targetPosition, titles.timing.position)
    await this.milis(titles.timing.wait.middle)
    await this.fadeOut(titles.timing.fadeOut)
    await this.milis(titles.timing.wait.end)
  }

  async milis (waitFor: number) {
    if (this.stateChange) this.stateChange('start', 'wait', waitFor)
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (this.stateChange) this.stateChange('completed', 'wait')
        resolve()
      }, waitFor)
    })
  }

  async fadeIn (duration: number = 300) {
    if (this.titleHasChanged) this.titleHasChanged(titles.languages[this.languageIndex], flags[titles.languages[this.languageIndex]])
    if (this.stateChange) this.stateChange('start', 'fadeIn', duration)
    return new Promise((resolve, reject) => {
      this.animation = anime({
        targets: titlesView.titleDiv,
        opacity: 1,
        duration: duration,
        easing: 'easeInOutSine',
        complete: () => {
          if (this.stateChange) this.stateChange('completed', 'fadeIn')
          this.animation = null
          resolve(true)
        }
      })
    })
  }

  async fadeOut (duration: number = 300) {
    if (this.stateChange) this.stateChange('start', 'fadeOut', duration)
    return new Promise((resolve, reject) => {
      this.animation = anime({
        targets: titlesView.titleDiv,
        opacity: 0,
        duration: duration,
        easing: 'easeInOutSine',
        complete: () => {
          if (this.stateChange) this.stateChange('completed', 'fadeOut')
          this.animation = null
          resolve(true)
        }
      })
    })
  }

  setPosition (pos: { x: number, y: number }, duration: number = 4000) {
    if (this.stateChange) this.stateChange('start', 'setPosition', duration)
    return new Promise((resolve, reject) => {
      this.animation = anime({
        targets: titlesView.titleDiv,
        translateX: pos.x,
        translateY: pos.y,
        translateZ: 0,
        easing: 'easeInOutSine',
        duration: duration,
        complete: () => {
          if (this.stateChange) this.stateChange('completed', 'setPosition')
          this.animation = null
          resolve(true)
        }
      })
    })
  }

  getVDistance (pos: { x: number, y: number }) {
    const horizontalDiff = pos.x - this.lastPosition.x
    const verticalDiff = pos.y - this.lastPosition.y
    return Math.sqrt(Math.pow(horizontalDiff, 2) + Math.pow(verticalDiff, 2))
  }

  getRandomPosition (shouldBeFar: boolean = false): { x: number, y: number } {
    let x = this.getRandomNumber()
    let y = (Math.random() * (1080 - titles.navbarHeight))
    x = map(x, 0, 1920, titles.padding, 1920)
    y = map(y, 0, 1080 - titles.navbarHeight, titles.padding, 1080 - (titles.padding + titles.navbarHeight))
    if (shouldBeFar && this.getVDistance({ x: x, y: y }) < 200) {
      return this.getRandomPosition(shouldBeFar)
    }
    this.lastPosition = { x: x, y: y }
    return { x, y }
  }

  getRandomNumber () {
    return Math.floor((Math.random() * (1920 - (this.getWidth() + (titles.padding * 2)))))
  }

  getWidth () {
    return titlesView.titleDiv.offsetWidth
  }
}

const map = function (value: number, inMin: number, inMax: number, outMin: number, outMax: number) {
  return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin
}

const flags = {
  nl: '🇧🇪',
  en: '🇬🇧',
  fr: '🇫🇷',
  de: '🇩🇪'
}

export const titlesController = new TitlesController()
