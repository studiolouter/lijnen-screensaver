import { AnimationSettings } from './timings'

interface VerstoringenTiming {
  lijnen_links: AnimationSettings,
  lijnen_rechts: AnimationSettings,
  circles01: AnimationSettings,
  circles02: AnimationSettings,
  circles03: AnimationSettings,
  circles04: AnimationSettings,
  circles05: AnimationSettings,
  circles06: AnimationSettings,
  circles07: AnimationSettings,
  // layer_3: AnimationSettings,
  // layer_4: AnimationSettings
}

export const verstoringenTiming: VerstoringenTiming = {
  lijnen_links: {
    delay: {
      stagger: 300,
      before: 3000,
      firstTime: 0
    },
    break: 0,
    duration: 2700
  },
  lijnen_rechts: {
    delay: {
      stagger: 300,
      before: 3000,
      firstTime: 0
    },
    break: 0,
    duration: 12000
  },
  circles01: {
    delay: {
      stagger: 26,
      before: 5000,
      firstTime: 3000
    },
    break: 1600,
    duration: 250,
    type: 'opacity'
  },
  circles02: {
    delay: {
      stagger: 200,
      before: 0,
      firstTime: 0
    },
    break: 0,
    duration: 500,
    type: 'opacity'
  },
  circles03: {
    delay: {
      stagger: 200,
      before: 0,
      firstTime: 0
    },
    break: 0,
    duration: 500,
    type: 'opacity'
  },
  circles04: {
    delay: {
      stagger: 200,
      before: 0,
      firstTime: 0
    },
    break: 0,
    duration: 500,
    type: 'opacity'
  },
  circles05: {
    delay: {
      stagger: 200,
      before: 0,
      firstTime: 0
    },
    break: 0,
    duration: 500,
    type: 'opacity'
  },
  circles06: {
    delay: {
      stagger: 200,
      before: 0,
      firstTime: 0
    },
    break: 0,
    duration: 500,
    type: 'opacity'
  },
  circles07: {
    delay: {
      stagger: 200,
      before: 0,
      firstTime: 0
    },
    break: 0,
    duration: 500,
    type: 'opacity'
  }
  // layer_2: {
  //   delay: {
  //     stagger: 5,
  //     before: 3000,
  //     firstTime: 0
  //   },
  //   break: 0,
  //   duration: 2600
  // },
  // layer_3: {
  //   delay: {
  //     stagger: 5,
  //     before: 3000,
  //     firstTime: 0
  //   },
  //   break: 0,
  //   duration: 2600
  // },
  // layer_4: {
  //   delay: {
  //     stagger: 5,
  //     before: 3000,
  //     firstTime: 0
  //   },
  //   break: 0,
  //   duration: 2600
  // }
}
