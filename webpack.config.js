const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// const CopyPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: path.join(__dirname, '/src/index.ts'),
  output: {
    filename: 'dist/index.js',
    publicPath: '',
    path: path.resolve(__dirname),
    library: 'lijnenscreensaver',
    libraryTarget: 'umd',
    umdNamedDefine: true
  },
  mode: 'production',
  target: 'node',
  node: {
    __dirname: false
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }, {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          publicPath: '../../../../../app/dist/frontend/static/',
          name: 'images/[name].[ext]',
          outputPath: 'app/dist/frontend/static'
        }
      },
      {
        test: /\.(woff2)$/,
        loader: 'url-loader',
        options: {
          limit: Infinity,
        },
      },
      { test: /\.svg$/, loader: 'raw-loader' }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  plugins: [
  ],
  watch: false
}
