import { AnimationSettings } from './timings'

interface HabitatTiming {
  lijnen0: AnimationSettings,
  lijnen1: AnimationSettings,
  // lijnen2: AnimationSettings,
  // circles: AnimationSettings,
}

export const habitatTiming: HabitatTiming = {
  lijnen0: {
    delay: {
      stagger: 50,
      before: 0,
      firstTime: 0
    },
    break: 0,
    duration: 1000,
    wait: 16900,
    hasCircles: true,
    circleBackAnimation: {
      stagger: 50,
      duration: 1000
    }
  },
  lijnen1: {
    delay: {
      stagger: 50,
      before: 0,
      firstTime: 10500
    },
    break: 0,
    duration: 1000,
    wait: 10500,
    hasCircles: true,
    circleBackAnimation: {
      stagger: 50,
      duration: 1000
    }
  }
  // lijnen2: {
  //   delay: {
  //     stagger: 50,
  //     before: 0,
  //     firstTime: 27400
  //   },
  //   break: 0,
  //   duration: 1000,
  //   wait: 27400,
  //   // type: 'opacity',
  //   hasCircles: true,
  //   circleBackAnimation: {
  //     stagger: 50,
  //     duration: 1000
  //   }
  // },
  // circles: {
  //   delay: {
  //     stagger: 125,
  //     before: 0,
  //     firstTime: 0
  //   },
  //   break: 0,
  //   duration: 130,
  //   type: 'opacity',
  //   hasCircles: true,
  //   circleBackAnimation: {
  //     stagger: 10,
  //     duration: 1000
  //   }
  // }
}
