import { overvloedTiming } from './overvloed'
import { verstoringenTiming } from './verstoringen'
import { verwantschappenTiming } from './verwantschappen'
import { habitatTiming } from './habitat'
import { interactiesTiming } from './interacties'

class Timings {
  setTimingOfZone (zone: string) {
    switch (zone) {
      case 'overvloed':
        Object.keys(overvloedTiming).forEach(timingKey => {
          this[timingKey] = overvloedTiming[timingKey]
        })
        break
      case 'verwantschappen':
        Object.keys(verwantschappenTiming).forEach(timingKey => {
          this[timingKey] = verwantschappenTiming[timingKey]
        })
        break
      case 'verstoringen':
        Object.keys(verstoringenTiming).forEach(timingKey => {
          this[timingKey] = verstoringenTiming[timingKey]
        })
        break
      case 'habitats':
        Object.keys(habitatTiming).forEach(timingKey => {
          this[timingKey] = habitatTiming[timingKey]
        })
        break
      case 'interacties':
        Object.keys(interactiesTiming).forEach(timingKey => {
          this[timingKey] = interactiesTiming[timingKey]
        })
        break
      default:
        throw new Error('Deze zone ken ik niet')
    }
  }
}

export interface AnimationSettings {
  delay: {
    stagger: number,
    before: number,
    firstTime: number
  },
  duration: number,
  break: number,
  wait?: number,
  type?: string,
  hasCircles?: boolean,
  circleBackAnimation?: {
    stagger: number,
    duration: number
  }
}

export const timings = new Timings()
