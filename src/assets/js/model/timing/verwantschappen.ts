import { AnimationSettings } from './timings'

interface VerwantschappenTiming {
  lijnen0: AnimationSettings,
  lijnen1: AnimationSettings,
  lijnen2: AnimationSettings,
  lijnen3: AnimationSettings,
}

export const verwantschappenTiming: VerwantschappenTiming = {
  lijnen0: {
    delay: {
      stagger: 25,
      before: 21500,
      firstTime: 0
    },
    break: 0,
    duration: 1000,
    wait: 16900,
    hasCircles: true,
    circleBackAnimation: {
      stagger: 50,
      duration: 1000
    }
  },
  lijnen1: {
    delay: {
      stagger: 25,
      before: 21500,
      firstTime: 7500
    },
    break: 0,
    duration: 1000,
    wait: 10500,
    hasCircles: true,
    circleBackAnimation: {
      stagger: 50,
      duration: 1000
    }
  },
  lijnen2: {
    delay: {
      stagger: 25,
      before: 21500,
      firstTime: 15000
    },
    break: 0,
    duration: 1000,
    wait: 10500,
    hasCircles: true,
    circleBackAnimation: {
      stagger: 50,
      duration: 1000
    }
  },
  lijnen3: {
    delay: {
      stagger: 25,
      before: 21500,
      firstTime: 22500
    },
    break: 0,
    duration: 1000,
    wait: 10500,
    hasCircles: true,
    circleBackAnimation: {
      stagger: 50,
      duration: 1000
    }
  }
}
