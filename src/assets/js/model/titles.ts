class Titles {
  data: ITitles
  languages: Array<string>
  padding: number
  navbarHeight: number
  multiplier: number
  timing: {
    wait: {
      start: number,
      middle: number,
      end: number
    },
    fadeIn: number,
    position: number,
    fadeOut: number
  }
  animationSequence: Array<string>

  constructor () {
    this.data = {
      nl: null,
      en: null,
      de: null,
      fr: null
    }
    this.languages = [ 'nl', 'en', 'de', 'fr' ]
    this.padding = 20
    this.navbarHeight = 300
    this.multiplier = 1
    this.timing = {
      wait: {
        start: 300 * this.multiplier,
        middle: 100 * this.multiplier,
        end: 2000 * this.multiplier
      },
      fadeIn: 600 * this.multiplier,
      position: 4000 * this.multiplier,
      fadeOut: 600 * this.multiplier
    }
    this.animationSequence = ['fadeIn', 'wait.start', 'setPosition', 'wait.middle', 'fadeOut', 'wait.end']
  }

  setup (titles: ITitles) {
    this.data.nl = titles.nl
    this.data.en = titles.en
    this.data.de = titles.de
    this.data.fr = titles.fr
    return this
  }

  setPadding (padding: number) {
    this.padding = padding
  }
}

export interface ITitles {
  nl: string,
  en: string,
  fr: string,
  de: string
}

export const titles = new Titles()
